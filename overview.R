###########################################################################################
### OVERVIEW
###########################################################################################
overView <- function() {
  query <- paste0(
    "PREFIX gbol: <http://gbol.life/0.1/>
    SELECT ?graph ?biom (COUNT(DISTINCT(?library)) AS ?libCounts) (COUNT(DISTINCT(?sampleName)) AS ?sampleCounts) (COUNT(DISTINCT(?asvid)) AS ?asvCounts) ?totalReads
    WHERE {
      GRAPH ?graph {
        ?library gbol:provenance ?prot .
        ?prot gbol:annotation ?annot .
        ?annot gbol:biomFile ?biom .
        ?library gbol:sample ?sample .
        ?library gbol:totalReads ?totalReads .
        ?sample gbol:name ?sampleName .
        ?sample gbol:asv ?asv .
        ?asv a gbol:ASVSet .
        ?asv gbol:assignedTaxon ?tax .
        ?asv gbol:masterASVId ?asvid .
    }} GROUP BY ?graph ?biom ?totalReads"
  )
  dfGraph <- SPARQL(endpoint, query)
  if (dim(dfGraph)[1] == 0) {
    return (dfGraph)
  }
  colnames(dfGraph) <- c("GRAPH", "BIOMfile", "Number of library", "Number of sample", "Number of OTUs", "Total reads")
  dfGraph$BIOMfile <- sapply(str_split(dfGraph$BIOMfile, "/"), tail, 1)
  return(dfGraph)
}

###########################################################################################
### OTU table
###########################################################################################
otuTableOverview <- function() {
  query <- paste0(
    "PREFIX gbol: <http://gbol.life/0.1/>
     SELECT ?graph ?biom ?libNum ?sampleName ?taxonName ?clusteredReadCount
     WHERE {
      GRAPH ?graph {
        ?library gbol:provenance ?prot .
        ?prot gbol:annotation ?annot .
        ?annot gbol:biomFile ?biom .
        ?library gbol:sample ?sample .
        ?library gbol:libraryNum ?libNum .
        ?sample gbol:name ?sampleName .
        ?sample gbol:asv ?asv .
        ?asv a gbol:ASVSet .
        ?asv gbol:clusteredReadCount ?clusteredReadCount .
        ?asv gbol:assignedTaxon ?tax .
        ?tax gbol:taxonName ?taxonName .
     }}"
  )
  dfGraph <- SPARQL(endpoint, query)
  if (dim(dfGraph)[1] == 0) {
    return(dfGraph)
  }
  taxonomy <- str_split_fixed(dfGraph$taxonName, ";", 7)
  taxonomy = gsub("empty", "", taxonomy)
  taxonomy = gsub(" ", "_", taxonomy)
  # print(taxonomy)
  dfGraph <- cbind.data.frame(dfGraph$graph, dfGraph$biom, dfGraph$libNum, dfGraph$sampleName, taxonomy, dfGraph$clusteredReadCount, dfGraph$taxonName)
  colnames(dfGraph) <- c("GRAPH", "BIOMfile", "Library", "Sample", "Domain", "Phylum", "Class", "Order", "Family", "Genus", "Species", "ReadCounts", "Taxonomy")
  # print(dfGraph)
  dfGraph$BIOMfile <- sapply(str_split(dfGraph$BIOMfile, "/"), tail, 1)
  dfGraph$BIOMfile <- paste0(dfGraph$BIOMfile, "__#__", dfGraph$Sample)
  return(dfGraph)
}