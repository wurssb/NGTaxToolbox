# Rshiny - NGTax
#
# VERSION       0.1

FROM rocker/shiny:3.5.1

MAINTAINER Wasin Poncheewin, wasin.poncheewin@wur.nl

RUN rm -rf /srv/shiny-server/*
RUN apt-get update && apt-get install -y wget libcurl4-openssl-dev libssl-dev openjdk-8-jdk curl ruby procps 

ADD ./install_package.R /srv/shiny-server/

RUN su - -c "R -e \"source('/srv/shiny-server/install_package.R')\"" 

ADD ./*.R /srv/shiny-server/
ADD ./col_uniq.txt /srv/shiny-server/
ADD ./www/ /srv/shiny-server/www/

RUN mkdir /srv/shiny-server/temp && \
	mv /root/apache-jena-fuseki-3.10.0/ /srv/shiny-server/ && \
    mv /root/NGTax.jar /srv/shiny-server/ && \
    chown -R shiny:shiny /srv/shiny-server/

# # Expose port 80 (webserver), 21 (FTP server), 8800 (Proxy)
EXPOSE 3838

# # Autostart script that is invoked during container start
CMD ["/usr/bin/shiny-server.sh"]
