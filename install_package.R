chooseCRANmirror(graphics=FALSE, ind=2)

install_packages <- function() {
  .packages = c(
    # "ape",
    # "d3r",
    "data.table",
    # "data.tree",
    # "devtools",
    "dplyr",
    "DT",
    # "ggfortify",
    "ggplot2",
    # "ggpubr",
    "ggsci",
    "GUniFrac",
    "heatmaply",
    # "networkD3",
    # "pdp",
    "phangorn",
    "plotly",
    # "plyr",
    "RCurl",
    # "reshape",
    # "sankeytreeR",
    # "scales",
    "seqinr",
    "shiny",
    "shinyWidgets",
    # "shinyjs",
    "stringr",
    # "treemap",
    "tools",
    "vegan",
    "venn"
  )
  
  .bioc_packages <- c(
    # "picante",
    # "d3heatmap",
    # "phangorn"
  )
  
  # Check if package(s) were installed.
  .inst <- .packages %in% installed.packages()
  
  # Install CRAN packages (if not already installed)
  if (length(.packages[!.inst]) > 0) {
    install.packages(.packages[!.inst])
  }
  
  .inst <- .bioc_packages %in% installed.packages()
  if (any(!.inst)) {
    source("http://bioconductor.org/biocLite.R")
    biocLite(.bioc_packages[!.inst])
  }
  
  for (package in .packages) {
    library(package, character.only = T)
  }
  message("If there was no error then you are ready to do data analysis")
}

install_packages()

# Create temp folder
dir.create(path = paste0("./temp/"), showWarnings = F)

# Download NGTax.jar
if(!file.exists("NGTax.jar")){
  res <- tryCatch(download.file("http://download.systemsbiology.nl/ngtax/NGTax-2.1.17.jar",
                                destfile = "NGTax.jar",
                                method = "auto"),
                  error = function(e) 1)
}

# Download Fuseki
if(!file.exists("apache-jena-fuseki-3.10.0/")){
  res <- tryCatch(download.file("http://apache.mirror1.spango.com/jena/binaries/apache-jena-fuseki-3.10.0.tar.gz",
                                destfile = "apache-jena-fuseki-3.10.0.tar.gz",
                                method = "auto"),
                  error = function(e) 1)
  system("tar -xvzf apache-jena-fuseki-3.10.0.tar.gz")
}

rm(list = ls())
gc()

